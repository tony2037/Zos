#ifndef __GDT_H
#define __GDT_H

#include "types.h"
class GlobalDescriptorTable{
    public:
    // for an entry
        class SegmentDescriptor
        {
        private:
        /*
        8 bytes totally (64 bits)
        */
            uint16_t limit_lo;
            uint16_t base_lo;
            uint8_t base_hi;
            uint8_t type;
            uint8_t flags_limit_hi;
            uint8_t base_vhi;
        public:
            SegmentDescriptor(uint32_t base, uint32_t limit, uint8_t type);
            uint32_t Base();// Turn the pointer to the limit
            uint32_t Limit();
        } __attribute__((packed));

        // an empty Segment
        SegmentDescriptor nullSegmentSelector;
        // an unused Segment
        SegmentDescriptor unusedSegmentSelector;
        // a code Segment
        SegmentDescriptor codeSegmentSelector;
        // a data Segment
        SegmentDescriptor dataSegmentSelector;

    public:
        GlobalDescriptorTable();
        ~GlobalDescriptorTable();

        uint16_t CodeSegmentSelector();// Give us the offset of code segment
        uint16_t DataSegmentSelector();// Give us the offset of data segment

};

#endif