#include "types.h"
#include "gdt.h"
#include "interrupts.h"


void printf(char* str)
{
    static uint16_t* VideoMemory = (uint16_t*)0xb8000;

    // The screen width : 80, height : 25
    static uint8_t x = 0, y = 0;    // The axis of the screen
    
    for(int i = 0 ; str[i] != '\0'; ++i)
    {
        switch(str[i])
        {
            // if \n then get to next line
            case '\n':
                ++y;
                x = 0;
                break;
            default:
                VideoMemory[80*y + x] = (VideoMemory[80*y + x] & 0xFF00) | str[i];
                ++x;
                break;
        }
        

        // if get to the right edge of the screen
        if(x >= 80)
        {
            ++y;
            x = 0;
        }
        // if get to the bottom of the screen
        // I just clean up the hole screen XD
        if( y >= 25)
        {
            for(y = 0; y < 25; y++)
                for(x = 0; x < 80; x++)
                    VideoMemory[80*y + x] = (VideoMemory[80*y + x] & 0xFF00) | ' ';
            x = 0;
            y = 0;
        }
    }
}

extern "C" void kernelMain(void* multiboot_structure, uint32_t magicnumber)
{
    printf("Hello Ztex\n");
    printf("Ztex wanna be the best engineer");

    // Create table, activate hardware
    GlobalDescriptorTable gdt;
    InterruptManager interrupts(&gdt);

    // activate interrupt
    interrupts.Activate();

    while(1);
}