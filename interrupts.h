#ifndef __INTERRUPTS_H
#define __INTERRUPTS_H

#include "types.h"
#include "port.h"
#include "gdt.h"
    class InterruptManager
    {
    protected:
        // Gate Descriptor is the entry of IDT
        struct GateDescriptor
        {
            uint16_t handlerAddressLowBits;
            uint16_t gdt_codeSegmentSelector;
            uint8_t reserved;
            uint8_t access;
            uint16_t handlerAddressHighBits;
        }__attribute__((packed));

        static GateDescriptor interruptDescriptorTable[256];

        struct InterruptDescriptorTablePointer
        {
            uint16_t size;
            uint32_t base;
        }__attribute__((packed));

        static void SetInterruptDescriptorTableEntry(
            uint8_t interruptNumber,
            uint16_t codeSegmentSelectorOffset,
            void (*handler)(),
            uint8_t DescriptorPrivilegeLevel,
            uint8_t DescriptorType
        );

        // Tell pic to send the interrupt
        // There is master pic and slabe pic
        // Need to commucate with both of them
        Port8BitSlow picMasterCommand;
        Port8BitSlow picMasterData;
        Port8BitSlow picSlaveCommand;
        Port8BitSlow picSlaveData;

    public:

        InterruptManager(GlobalDescriptorTable* gdt);
        ~InterruptManager();
        
        // tell cpu sending the interrupt
        void Activate();

        static uint32_t handleInterrupt(uint8_t interruptNumber, uint32_t esp);

        // Ignore the interrupt request
        static void IgnoreInterruptRequest();
        // For timer interrupt
        static void HandleInterruptRequest0x00();
        // For keyboard interrupt
        static void HandleInterruptRequest0x01();
    };
#endif